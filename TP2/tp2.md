# TP2 pt. 1 : Gestion de service

## 1. Installation

🖥️ **VM web.tp2.linux**

Le serveur tourne sur le port 80.

```console
[bob@localhost ~]$ sudo ss -alnpt
State         Recv-Q        Send-Q                 Local Address:Port                 Peer Address:Port
Process
LISTEN        0             128                          0.0.0.0:22                        0.0.0.0:*
users:(("sshd",pid=832,fd=5))
LISTEN        0             128                                *:80                              *:*
users:(("httpd",pid=1476,fd=4),("httpd",pid=1475,fd=4),("httpd",pid=1474,fd=4),("httpd",pid=1472,fd=4))
LISTEN        0             128                             [::]:22                           [::]:*
users:(("sshd",pid=832,fd=7))
```

On voit à la ligne 4 que le service est actif (Active: active (running)).
```console
[bob@localhost ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
  Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
  Active: active (running) since Wed 2021-10-06 12:22:32 CEST; 1min 39s ago
    Docs: man:httpd.service(8)
Main PID: 1472 (httpd)
  Status: "Running, listening on: port 80"
  Tasks: 213 (limit: 4946)
  Memory: 24.2M
  CGroup: /system.slice/httpd.service
          ├─1472 /usr/sbin/httpd -DFOREGROUND
          ├─1473 /usr/sbin/httpd -DFOREGROUND
          ├─1474 /usr/sbin/httpd -DFOREGROUND
          ├─1475 /usr/sbin/httpd -DFOREGROUND
          └─1476 /usr/sbin/httpd -DFOREGROUND

Oct 06 12:22:32 localhost.localdomain systemd[1]: Starting The Apache HTTP Server...
Oct 06 12:22:32 localhost.localdomain httpd[1472]: AH00558: httpd: Could not reliably determine the server's f>Oct 06 12:22:32 localhost.localdomain systemd[1]: Started The Apache HTTP Server.
Oct 06 12:22:32 localhost.localdomain httpd[1472]: Server configured, listening on: port 80
```

Le serveur est lancé automatiquement.
```console
[bob@localhost ~]$ sudo systemctl status httpd
[sudo] password for bob:
● httpd.service - The Apache HTTP Server
  Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```

```console
<!doctype html>
<html>
<head>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <title>HTTP Server Test Page powered by: Rocky Linux</title>
  <style type="text/css">
    /*<![CDATA[*/

    html {
      height: 100%;
      width: 100%;
    }
      body {
background: rgb(20,72,50);

[...]

    <footer class="col-sm-12">
    <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
    <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
    </footer>

</body>
</html>
```

```console
[bob@localhost ~]$ sudo firewall-cmd --list-ports

[bob@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[bob@localhost ~]$ sudo firewall-cmd --reload
success
[bob@localhost ~]$ sudo firewall-cmd --list-ports
80/tcp
```
Le port est maintenant activé, il suffit de rentrer l'IP de la machine sur un navigateur pour que la page s'affiche.

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume
`sudo systemctl enable httpd`

- prouvez avec une commande qu'actuellement, le service est paramétré pour démarrer quand la machine s'allume

```console
[bob@localhost ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
  Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```console
[bob@localhost ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```console
[bob@localhost conf]$ cat httpd.conf | grep User
User apache
```

- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```console
[bob@localhost conf]$ ps -ef
[...]
apache       854     830  0 15:38 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       855     830  0 15:38 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache       856     830  0 15:38 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache       898     830  0 15:38 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
[...]
```

- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
- vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf
```console
[bob@localhost ~]$ cd /usr/share/testpage/
[bob@localhost testpage]$ ls -al
total 12
drwxr-xr-x.  2 root root   24 Oct  6 09:53 .
drwxr-xr-x. 91 root root 4096 Oct  6 09:53 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```
On peut voir que les permissions r sont attribuées pour tous les utilisateurs. L'utilisateur apache a donc les droits de lecture.

🌞 **Changer l'utilisateur utilisé par Apache**

- créez le nouvel utilisateur


- pour les options de création, inspirez-vous de l'utilisateur Apache existant
- le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
- servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut
```console
[bob@localhost ~]$ sudo useradd bobby -m -d /usr/share/httpd -s /sbin/nologin
useradd: warning: the home directory already exists.
Not copying any file from skel directory into it.
[bob@localhost ~]$ cat /etc/passwd | grep bobby
bobby:x:1001:1001::/usr/share/httpd:/sbin/nologin
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
- redémarrez Apache
- utilisez une commande `ps` pour vérifier que le changement a pris effet
```console
  root        1711       1  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
bobby       1713    1711  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
bobby       1714    1711  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
bobby       1715    1711  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
bobby       1716    1711  0 14:09 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
bob         1938    1675  0 14:10 pts/0    00:00:00 grep --color=auto httpd
[bob@localhost ~]$
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demande d'écouter sur un autre port de votre choix
```console
 [bob@localhost ~]$ sudo vi /etc/httpd/conf/httpd.conf
[bob@localhost ~]$ cat /etc/httpd/conf/httpd.conf | grep Listen
Listen 8080
```
- ouvrez un nouveau port firewall, et fermez l'ancien
```console
[bob@localhost ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[bob@localhost ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[bob@localhost ~]$ sudo firewall-cmd --reload
success
[bob@localhost ~]$ sudo firewall-cmd --list-all
[...]
  services: cockpit dhcpv6-client ssh
  ports: 8080/tcp
[...]
  rich rules:
```
- redémarrez Apache
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```console
[bob@localhost ~]$ sudo ss -antpl | grep httpd
LISTEN 0      128                *:8080            *:*    users:(("httpd",pid=2221,fd=4),("httpd",pid=2220,fd=4),("httpd",pid=2219,fd=4),("httpd",pid=2217,fd=4))
```
(franchement, c'est bien QUE pour ton tp que j'utilise ss, parce que bon, en vrai les SS c'est pas ouf)

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```console
[bob@localhost ~]$ curl localhost:8080
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        [...]
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port
```console
PS C:\Users\lucas> curl 10.102.1.11:8080
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a
Rocky Linux system. If you can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenanc
If you would like the let the administrators of this website know that you've seen this page inste
ad of the page you've expected, you should send them an email. In general, mail sent to the name "
webmaster" and directed to the website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
Note:
The Rocky Linux distribution is a stable and reproduceable platform based on the sources of Red Ha
t Enterprise Linux (RHEL). With this in mind, please understand that:
Neither the Rocky Linux Project nor the Rocky Enterprise Software Foundation have anything to do w
ith this website or its content.
The Rocky Linux Project nor the RESF have "hacked" this webserver: This test page is included with
 the distribution.
For more information about Rocky Linux, please visit the Rocky Linux website.
I am the admin, what do I do?
You may now add content to the webroot directory for your software.
For systems using the Apache Webserver: You can add content to the directory /var/www/html/. Until
 you do so, people visiting your website will see this page. If you would like this page to not be
 shown, follow the instructions in: /etc/httpd/conf.d/welcome.conf.
For systems using Nginx: You can add your content in a location of your choice and edit the root c
onfiguration directive in /etc/nginx/nginx.conf.

Apache™ is a registered trademark of the Apache Software Foundation in the United States and/or ot
her countries.
NGINX™ is a registered trademark of F5 Networks, Inc..
Au caractère Ligne:1 : 1
+ curl 10.102.1.11:8080
+ ~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidOperation : (System.Net.HttpWebRequest:HttpWebRequest) [Invo
   ke-WebRequest], WebException
    + FullyQualifiedErrorId : WebCmdletWebResponseException,Microsoft.PowerShell.Commands.InvokeW
   ebRequestCommand
```

sudo dnf install epel-release
[bob@localhost ~]$ sudo dnf update
[bob@localhost ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[bob@localhost ~]$ sudo dnf module list php
[bob@localhost ~]$ sudo dnf module enable php:remi-7.4


```console
[bob@localhost ~]$ sudo dnf module list php
Last metadata expiration check: 0:00:33 ago on Sun 10 Oct 2021 06:14:50 PM CEST.
Rocky Linux 8 - AppStream
Name       Stream             Profiles                         Summary
php        7.2 [d]            common [d], devel, minimal       PHP scripting language
php        7.3                common [d], devel, minimal       PHP scripting language
php        7.4                common [d], devel, minimal       PHP scripting language

Remi's Modular repository for Enterprise Linux 8 - x86_64
Name       Stream             Profiles                         Summary
php        remi-7.2           common [d], devel, minimal       PHP scripting language
php        remi-7.3           common [d], devel, minimal       PHP scripting language
php        remi-7.4 [e]       common [d], devel, minimal       PHP scripting language
php        remi-8.0           common [d], devel, minimal       PHP scripting language
php        remi-8.1           common [d], devel, minimal       PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
```

installation de mariadb sur db.tp2.linux 

```console
[bob@db ~]$ sudo dnf install mariadb-server
[bob@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[bob@db ~]$ sudo systemctl start mariadb
[bob@db ~]$ mysql_secure_installation
[bob@db ~]$ sudo ss -antpl
State  Recv-Q Send-Q Local Address:Port   Peer Address:Port Process
LISTEN 0      128          0.0.0.0:22          0.0.0.0:*     users:(("sshd",pid=836,fd=5))
LISTEN 0      128             [::]:22             [::]:*     users:(("sshd",pid=836,fd=7))
LISTEN 0      80                 *:3306              *:*     users:(("mysqld",pid=8184,fd=21))
[bob@db ~]$ sudo ss -antpl
State  Recv-Q Send-Q Local Address:Port   Peer Address:Port Process
LISTEN 0      128          0.0.0.0:22          0.0.0.0:*     users:(("sshd",pid=836,fd=5))
LISTEN 0      128             [::]:22             [::]:*     users:(("sshd",pid=836,fd=7))
LISTEN 0      80                 *:3306              *:*     users:(("mysqld",pid=8184,fd=21))

[bob@db ~]$ sudo firewall-cmd --new-zone=mariadb-access --permanent
success
[bob@db ~]$ sudo firewall-cmd --reload
success
[bob@db ~]$ sudo firewall-cmd --get-zones
block dmz drop external home internal mariadb-access nm-shared public trusted work
[bob@db ~]$ sudo firewall-cmd --zone=mariadb-access --add-source=10.102.1.11/24 --permanent
success
[bob@db ~]$ sudo firewall-cmd --zone=mariadb-access --add-port=3306/tcp --permanent
success
[bob@db ~]$ sudo firewall-cmd --reload
success
```

on termine de configurer bd.tp2.linux pour les databases.

```console
[bob@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 33
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

on se connecte à la database depuis web.tp2.linux
```console
[bob@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 39
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.002 sec)

MariaDB [(none)]> USE nextcloud
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```


| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | toutes        |
| `db.tp2.linux` | `10.102.1.12`  | Base de Données         | 3306        | `10.102.1.12` |

> Ce tableau devra figurer à la fin du rendu, avec les ? remplacés par la bonne valeur (un seul tableau à la fin). Je vous le remets à chaque fois, à des fins de clarté, pour lister les machines qu'on a à chaque instant du TP.
