# TP 1 : Familiaration avec un système GNU/Linux

## 0.Préparation de la machine

🎄(il est beau ce sapin n'est-ce pas ?) Juste avant la fin du tp, je me suis rendu compte que j'avais pas configuré mes VMS dans le réseau que tu avais demandé. Tout fonctionne correctement, mais les IPs des VMS ne sont pas celles du tableau. 
node1.tp1.b2 = 10.2.1.145
node2.tp1.b2 = 10.2.1.146
HostOnly = 10.2.1.15

* Accès internet :
    * On fait la commande `ip a`, afin d'obtenir les différentes cartes réseaux. On voit la carte réseau `enp0s3`, qui est la carte NAT.
    * On utilise la commande `ip r s` pour afficher les routes. On voit la ligne ```default via 10.0.2.2 dev enp0s3 proto dhcp metric 101```, qui est la route par défaut vers Internet.
* un accès à un réseau local
    * On fait la commande `ip a ` afin d'obtenir les différentes cartes réseaux. On voit la carte réseau `enp0s8` qui est la carte Host-Only configurée. 
    * On modifie le fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s8` de chaque VM, en rajoutant une IP présente dans le réseau. (10.2.1.145 et 10.2.1.146 dans mon cas)

* les machines doivent avoir un nom
    * On modifie le fichier `/etc/hostname` de chaque machine avec nano, et on met le nom de notre choix. on fait ensuite un `reboot now` pour que le changement de nom opère. 

    ```console
    [bob@VMLinux ~]$ cat /etc/hostname
    node1.tp1.b2
    -----------------
    [bob@node2 ~]$ cat /etc/hostname
    node2.tp1.b2
    ```

* Utiliser 1.1.1.1 comme serveur DNS

    * On modifie le fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s3` en rajoutant la ligne `DNS=1.1.1.1`. On fait `sudo nmcli con reload` pour que le changement opère, et on fait la commande `dig <site internet>` pour vérifier, et : 
    ```console
    [bob@node2 ~]$ dig ynov.com

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31181
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1220
    ; COOKIE: ca3b22271f8dbfe8fdc3b4a7615339a9ecc99b838d8742b1 (good)
    ;; QUESTION SECTION:
    ;ynov.com.                      IN      A

    ;; ANSWER SECTION:
    ynov.com.               10800   IN      A       92.243.16.143

    ;; Query time: 20 msec
    ;; SERVER: 192.168.1.254#53(192.168.1.254)
    ;; WHEN: Tue Sep 28 17:49:58 CEST 2021
    ;; MSG SIZE  rcvd: 81

    ```

    * L'ip destination est : `92.243.16.143`
    * Le serveur qui nous a répondu se trouve ici `SERVER: 192.168.1.254#53(192.168.1.254)`
* les machines doivent pouvoir se joindre par leurs noms respectifs
    * On modifie le fichier `etc/hosts` avec nano, et on ajoute la ligne `<ipAutreMachine> <nomAutreMachine` 
    Une fois modifiés, on peut ping d'une machine à l'autre.
    ```console
    [bob@VMLinux ~]$ ping node2.tp1.b2
    PING node2.tp1.b2 (10.2.1.146) 56(84) bytes of data.
    64 bytes from node2.tp1.b2 (10.2.1.146): icmp_seq=1 ttl=64 time=0.516 ms
    64 bytes from node2.tp1.b2 (10.2.1.146): icmp_seq=2 ttl=64 time=0.580 ms
    64 bytes from node2.tp1.b2 (10.2.1.146): icmp_seq=3 ttl=64 time=0.470 ms
    [...]
    ^C
    --- node2.tp1.b2 ping statistics ---
    7 packets transmitted, 7 received, 0% packet loss, time 6128ms
    rtt min/avg/max/mdev = 0.470/0.537/0.610/0.048 ms
    ```
* le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires
    ```console
    [bob@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
    [sudo] password for bob:
    success
    [bob@node1 ~]$ sudo firewall-cmd --reload
    success
    ```

## I.Utilisateurs

### 1. Création et configuration

* Il faut utiliser la commande `sudo useradd Bob -m s /bin/bash` pour créer un user "bob" avec un répertoire dans le /home et .
```console
[bob@VMLinux ~]$ 
[bob@VMLinux ~]$ pwd
/home/bob
[bob@VMLinux ~]$ cd ..
[bob@VMLinux home]$ pwd
/home
[bob@VMLinux home]$ ls
bob  Bob
[bob@VMLinux home]$
```
On voit que mon nouvel utilisateur Bob a son répertoire directement dans /home.

* Pour créer un nouveau groupe admins, on fait : 
```console
[bob@node2 ~]$ sudo groupadd admins
[bob@node2 ~]$ sudo visudo /etc/sudoers

# on ajoute cette ligne à l'intérieur du fichier.
%admins ALL=(ALL) ALL
```

On fait `[bob@node2 ~]$ sudo usermod -aG admins Bob` pour ajouter l'utilisateur Bob au groupe admins.

## II. Partitionnement

Pour agréger les deux disques en un seul volume group, on fait : 
```console
[bob@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
[bob@node1 ~]$ sudo pvcreate /dev/sdb
[sudo] password for bob:
  Physical volume "/dev/sdb" successfully created.
[bob@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[bob@node1 ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   3.00g 3.00g
  /dev/sdc      lvm2 ---   3.00g 3.00g
[bob@node1 ~]$ sudo vgcreate data /dev/sdb
  Volume group "data" successfully created
[bob@node1 ~]$ sudo vgextend data /dev/sdc
  Volume group "data" successfully extended
```

Pour créer 3 logical volumes de 1 Go chacun, on fait : 
```console
[bob@node1 ~]$ sudo lvcreate -L 1G data -n one_data
  Logical volume "one_data" created.
[bob@node1 ~]$ sudo lvcreate -L 1G data -n two_data
  Logical volume "two_data" created.
[bob@node1 ~]$ sudo lvcreate -L 1G data -n three_data
  Logical volume "three_data" created.
[bob@node1 ~]$ sudo lvs
  LV         VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  one_data   data -wi-a-----   1.00g
  three_data data -wi-a-----   1.00g
  two_data   data -wi-a-----   1.00g
  root       rl   -wi-ao----  <6.20g
  swap       rl   -wi-ao---- 820.00m
```

Pour formater les partitions, on utilise la commande suivante (pour chacune des partitions) : 
```console
[bob@node1 ~]$ sudo mkfs -t ext4 /dev/data/one_data
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 18433a10-7902-42b6-9a22-883e409ed2c8
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

Désormais, il faut les rendre accessible aux points de routage /mnt/ part1|part2|part3

```console
[bob@node1 ~]$ sudo mkdir /mnt/data1
[bob@node1 ~]$ sudo mkdir /mnt/data2
[bob@node1 ~]$ sudo mkdir /mnt/data3
[bob@node1 ~]$ sudo mount /dev/data/one_data /mnt/data1
[bob@node1 ~]$ sudo mount /dev/data/two_data /mnt/data2
[bob@node1 ~]$ sudo mount /dev/data/three_data /mnt/data3
```

Puis, on fait la commande `df -h`, et on obtient :
```console
[bob@node1 ~]$ df -h
Filesystem                   Size  Used Avail Use% Mounted on
devtmpfs                     880M     0  880M   0% /dev
tmpfs                        898M     0  898M   0% /dev/shm
tmpfs                        898M  8.5M  890M   1% /run
tmpfs                        898M     0  898M   0% /sys/fs/cgroup
/dev/mapper/rl-root          6.2G  2.0G  4.2G  33% /
/dev/sda1                   1014M  241M  774M  24% /boot
tmpfs                        180M     0  180M   0% /run/user/1000
/dev/mapper/data-one_data    976M  2.6M  907M   1% /mnt/data1
/dev/mapper/data-two_data    976M  2.6M  907M   1% /mnt/data2
/dev/mapper/data-three_data  976M  2.6M  907M   1% /mnt/data3
```

Pour faire en sorte que la partition soit montée automatiquement au démarrage du système, on fait :
```console
[bob@node1 ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/data1 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/data1               : successfully mounted
mount: /mnt/data2 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/data2               : successfully mounted
mount: /mnt/data3 does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/data3               : successfully mounted
```

